﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using ShareAPI.Models;
using System.Web.Http;
using System.Net.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ShareAPI.Controllers
{
    
    public class LinkController : ApiController
    {
        private LinkDb db = new LinkDb();
        private UserManager<ApplicationUser> manager;

        public LinkController()
        {
            manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
        }
        //// GET: Link
        //public ActionResult Index()
        //{
        //    return View(db.Links.ToList());
        //}

        //// GET: Link/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Link link = db.Links.Find(id);
        //    if (link == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(link);
        //}

        // POST: Link/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        public HttpResponseMessage Create([System.Web.Mvc.Bind(Include = "Url")] Link link)
        {
            HttpResponseMessage response; 

            if (ModelState.IsValid)
            {
                if (link != null)
                {
                    link.Created = DateTime.Now;
                    link.Likes = 0;
                    link.Clicks = 0;

                    db.Links.Add(link);
                    db.SaveChanges();

                    response = Request.CreateResponse(HttpStatusCode.OK, link);
                    return response;
                }
            }

            response = Request.CreateResponse(HttpStatusCode.InternalServerError, ModelState);
            return response;
        }

        [HttpGet]
        public HttpResponseMessage Get(int? id = null)
        {
            List<Link> links = new List<Link>();

            if (id.HasValue)
            {
                links.Add(db.Links.Find(id));
            }
            else
            {
                links = db.Links.ToList();
            }

            if (links != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, links);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError);
        }



        //// GET: Link/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Link link = db.Links.Find(id);
        //    if (link == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(link);
        //}

        //// POST: Link/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Url,UserName")] Link link)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(link).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(link);
        //}

        //// GET: Link/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Link link = db.Links.Find(id);
        //    if (link == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(link);
        //}

        //// POST: Link/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Link link = db.Links.Find(id);
        //    db.Links.Remove(link);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
