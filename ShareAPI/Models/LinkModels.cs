namespace ShareAPI.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    public class LinkDb : DbContext
    {
        // Your context has been configured to use a 'Link' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ShareAPI.Models.Link' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'Link' 
        // connection string in the application configuration file.
        public LinkDb()
            : base("name=Link")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Link> Links { get; set; }
    }

    public class Link
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Url { get; set; }
        
        public DateTime Created { get; set; }

        // Stats
        public int Clicks { get; set; }
        public int Likes { get; set; }
    }

}